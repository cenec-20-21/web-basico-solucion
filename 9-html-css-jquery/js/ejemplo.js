$(document).ready(()=>{
    // envuelvo el documento con jQuery y hago que todo lo que se ejecute se haga solamente cuando el documento haya cargado completamente
    let parrafo = $("#miTexto");
    parrafo.click(() => {
        parrafo.text('Hola mundo interactivo');
    });
});
